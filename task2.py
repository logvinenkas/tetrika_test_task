# Описание: В нашей школе мы не можем разглашать персональные данные пользователей, но чтобы преподаватель и ученик
# смогли объяснить нашей поддержке, кого они имеют в виду (у преподавателей, например, часто учится несколько Саш),
# мы генерируем пользователям уникальные и легко произносимые имена. Имя у нас состоит из прилагательного,
# имени животного и двузначной цифры. В итоге получается, например, "Перламутровый лосось 77". Для генерации таких
# имен мы и решали следующую задачу: Получить с русской википедии список всех животных (https://inlnk.ru/jElywR) и
# вывести количество животных на каждую букву алфавита. Результат должен получиться в следующем виде:
# А: 642
# Б: 412
# В:....

# Необходимо установить зависимости из файла requirements.txt

import os
from sys import platform
from bs4 import BeautifulSoup
import requests

if __name__ == '__main__':

    while True:
        answer = input('Выводить в консоль прогресс? (Д/н): ').lower()
        if answer in ['да', 'д', 'y', 'yes']:
            stats = True
            break
        elif answer in ['нет', 'н', 'n', 'no']:
            stats = False
            break
        else:
            print('Неверный ввод.', end=' ')

    os.environ.setdefault('TERM', 'xterm')
    url = 'https://ru.wikipedia.org'
    animals = {}
    letter = None
    counter = 0
    next_page_params = '/w/index.php?title=Категория%3AЖивотные_по_алфавиту'
    while True:
        animals_html = list()
        request = requests.post(url + next_page_params)
        if request.status_code == 200:
            soup = BeautifulSoup(request.text, "html.parser")
            animals_html = soup.find('div', id='mw-pages').find_all('li')
            if soup.find('div', id='mw-pages').find('a', text='Следующая страница'):
                next_page_params = soup.find('div', id='mw-pages').find('a', text='Следующая страница')['href']
            else:
                next_page_params = None
        else:
            print('Ошибка при получении ответа от сайта')
            break

        for item in animals_html:
            letter = str(item.find('a').text)[0]
            if animals.get(letter) is not None:
                animals[letter] += 1
            else:
                animals[letter] = 1

        # time.sleep(.5)

        if stats:
            try:
                if platform.startswith('linux'):
                    os.system('clear')
                elif platform.startswith('win32'):
                    os.system('cls')
                elif platform.startswith('darwin'):
                    os.system("printf '\\33c\\e[3J'")
                else:
                    raise Exception
            except Exception as e:
                print(f'Упс! Обновление окна консоли не работает на данной операционной системе')
            print(f'Обработано животных: {sum(animals.values())} шт.')

        if next_page_params is None:
            break

    for key, value in animals.items():
        print(f'{key}:\t{value}')
